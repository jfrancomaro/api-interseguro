package com.example.demo.service;

import com.example.demo.entity.Empleado;

public interface EmpleadoService {

	public Empleado save(Empleado empleado);
		
	public String getAllEmpleado();

}
