package com.example.demo.entity;

import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.constraints.Email;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@Document(collection = "Empleado")
public class Empleado {

	private static Integer contador = 100000;
	@Field(value = "codigo")
	private Integer		codigo;
	@Field(value = "nombre")
    private String 		nombre;
	@Field(value = "apellidoPaterno")
	private String 		apellidoPaterno;
	@Field(value = "apellidoMaterno")
    private String 		apellidoMaterno;
	@Field(value = "fechaNacimiento")
    private LocalDate	fechaNacimiento;
	@Email
	@Field(value = "correo")
    private String		correo;
	@Field(value = "categoria")
    private String		categoria;
	@Field(value = "salario")
	private Double		salario;
	@Field(value = "conocimientos")
    private String[]	conocimientos = new String[9];
	
	public void setCodigo(Integer codigo) {
		this.codigo = ++contador;
		
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public void setCorreo(String correo) {
		Pattern pattern = Pattern
	            .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher mather = pattern.matcher(correo);
        if (mather.find() == true) {
        	this.correo = correo;
        }
        else {
        	this.correo = correo + "@gmail.com";
        }
	}

	public void setSalario(Double Salario) {
		this.salario = Salario;
		if (Salario <= 1999.0) {
			this.categoria = "No tiene Categoria";
		}
		else if (Salario <= 2999.0) {
			this.categoria = "D";
		} else if (Salario <= 4999.0) {
			this.categoria = "C";
		} else if (Salario <= 7999.0) {
			this.categoria = "B";
		} else if (Salario <= 15000.0) {
			this.categoria = "A";
		} else {
			this.categoria = "No tiene Categoria";
		}
		
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public void setConocimientos(String[] conocimientos) {
		if(conocimientos.length <= 10) {
			this.conocimientos = conocimientos;
		}
		else 
		{
			for(int i = 0; i < 10 ; i++) {
				this.conocimientos[i] = conocimientos[i];
			}
		}
	}
	@Override
	public String toString() {
		return codigo + " - " + nombre + " " + apellidoPaterno + " "+ apellidoMaterno + " - " + fechaNacimiento + " ("
				+ categoria + ")";
	}
	
	
	
}
