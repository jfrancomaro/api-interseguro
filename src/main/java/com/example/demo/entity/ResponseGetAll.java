package com.example.demo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ResponseGetAll {

	private String empleados;

	public ResponseGetAll(String empleados) {
		super();
		this.empleados = empleados;
	}	
	
}
