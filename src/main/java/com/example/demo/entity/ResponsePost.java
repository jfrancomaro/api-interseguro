package com.example.demo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ResponsePost {

	private Integer codigoRespuesta;
	private String	mensajeRespuesta;
	public ResponsePost(Integer codigoRespuesta, String mensajeRespuesta) {
		super();
		this.codigoRespuesta = codigoRespuesta;
		this.mensajeRespuesta = mensajeRespuesta;
	}
	
}
