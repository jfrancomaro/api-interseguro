package com.example.demo.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.entity.Empleado;

public interface EmpleadoDao extends MongoRepository<Empleado, Integer> {

	
	
}

