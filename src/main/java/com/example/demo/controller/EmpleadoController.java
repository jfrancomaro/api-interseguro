package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Empleado;
import com.example.demo.entity.ResponseGetAll;
import com.example.demo.entity.ResponsePost;
import com.example.demo.service.EmpleadoService;

@RestController
@RequestMapping("/talentfestapi/empleados")
public class EmpleadoController {
	
	@Autowired
	private EmpleadoService empleadoService;

	
	//Para Insertar Empleados
	@PostMapping(headers = {"Content-Type=application/json", "Accept=application/json"})
	public ResponsePost save(@RequestBody Empleado empleado) {
		empleadoService.save(empleado);
		return new ResponsePost(1,"El Empleado "+empleado.getCodigo()+" se ha registrado");
	}
	//Para Mostrar Todos Los Empleados en una Lista
	@GetMapping(headers = "Content-Type=application/json")
	public ResponseGetAll getAllEmpleado(){	
		empleadoService.getAllEmpleado();
		return new ResponseGetAll(empleadoService.getAllEmpleado());
	}
}
